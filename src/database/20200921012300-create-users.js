'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface.createTable('users', {
   id: {
     allowNull: false,
     autoIncrement: true,
     primaryKey: true,
     type: Sequelize.INTEGER,
   },
   discord_id: {
    allowNull: false,
    unique: true,
    type: Sequelize.STRING
   },
   wallet: {
     default: 500,
     type: Sequelize.INTEGER
   },
   timestamps: true,
   underscored: true

  }),

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('users');
  }
};
