module.exports = 
{
  "development": {
    "username": "postgres",
    "password": "masterkey",
    "database": "helliot_bot",
    "host": "127.0.0.1",
    "dialect": "pg"
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mysql"
  }
}
